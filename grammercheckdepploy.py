import pandas as pd
import numpy as np
import re
import matplotlib.pyplot as plt
import seaborn as sns
import warnings 
warnings.filterwarnings('ignore')
import pickle
from tensorflow.keras import layers
from tensorflow.keras import Model
import tensorflow as tf
import model
from flask import Flask,jsonify,request,redirect,url_for
import flask
from  tensorflow.keras.preprocessing.sequence import pad_sequences
import io
import urllib, base64
import os
from datetime import datetime


app = Flask(__name__)


##### PREPROCESSING ############
def remove_spaces(text):
    text = re.sub(r" '(\w)",r"'\1",text)
    text = re.sub(r" \,",",",text)
    text = re.sub(r" \.+",".",text)
    text = re.sub(r" \!+","!",text)
    text = re.sub(r" \?+","?",text)
    text = re.sub(" n't","n't",text)
    text = re.sub("[\(\)\;\_\^\`\/]","",text)
    
    return text

def decontract(text):
    text = re.sub(r"won\'t", "will not", text)
    text = re.sub(r"can\'t", "can not", text)
    text = re.sub(r"n\'t", " not", text)
    text = re.sub(r"\'re", " are", text)
    text = re.sub(r"\'s", " is", text)
    text = re.sub(r"\'d", " would", text)
    text = re.sub(r"\'ll", " will", text)
    text = re.sub(r"\'t", " not", text)
    text = re.sub(r"\'ve", " have", text)
    text = re.sub(r"\'m", " am", text)
    return text

def preprocess(text):
    text = re.sub("\n","",text)
    text = remove_spaces(text)   # REMOVING UNWANTED SPACES
    text = re.sub(r"\.+",".",text)
    text = re.sub(r"\!+","!",text)
    text = decontract(text)    # DECONTRACTION
    text = re.sub("[^A-Za-z0-9 ]+","",text)
    text = text.lower()
    return text   

## Plot for alpha values ## 
def plot( input_sent , output_sent , alpha ) :

    input_words = input_sent.split() 
    output_words = output_sent.split() 
    fig, ax = plt.subplots()
    sns.set_style("darkgrid")
    sns.heatmap(alpha[:len(input_words),:], xticklabels= output_words , yticklabels=input_words,linewidths=0.01)
    ax.xaxis.tick_top() 

    
    
    model1.load_weights("load/best.h5")
    print("####### MODEL LOADED ###########")
